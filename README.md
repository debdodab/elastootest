# Test Project for Elastoo

## Technical task

![](https://imgur.com/upQ7Uo1.png)

## Realisation

* #### Used python3.9, asynchronous FastAPI, and asyncio lock
* #### Implemented list and queue of tasks as in-memory models
* #### Used FastAPI's background task to create a worker
* #### Waiting for a timeout, adding number to list, and deleting task from the queue in a worker is synchronous

## Installation and launching

```
git clone https://gitlab.com/debdodab/elastootest.git
cd elastootest
docker-compose build
docker-compose run -d
```

### You can read and try API at http://localhost:8008/docs