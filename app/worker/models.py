from datetime import datetime


"""Pretend those are DB models"""


class List:
    """List of numbers"""
    numbers = []

    @classmethod
    def append(cls, num: int):
        """Add number num to list"""
        cls.numbers.append(num)

    @classmethod
    async def get_all(cls) -> list[int]:
        """Return list of numbers"""
        return cls.numbers


class Task:
    """Task to add number num to list after timeout of seconds"""
    __last_used_id = 0
    id: int
    create_time: datetime
    num: int
    timeout: int

    def __init__(self, num: int, timeout: int):
        self.__class__.__last_used_id += 1
        self.id = self.__class__.__last_used_id

        self.create_time = datetime.now()

        self.num = num
        self.timeout = timeout

    def __repr__(self):
        return f"Task {{id={self.id}, create_time={self.create_time}, num={self.num}, timeout={self.timeout}}}"


class Queue:
    queue = []

    @classmethod
    async def append(cls, task: Task):
        """Add task to queue"""
        cls.queue.append(task)

    @classmethod
    async def get_all(cls) -> list[Task]:
        """Return queue sorted by create_time"""
        return sorted(cls.queue, key=lambda task: task.create_time)

    @classmethod
    def remove(cls, task: Task):
        """Remove task from queue"""
        cls.queue.remove(task)
