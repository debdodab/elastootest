import logging
from asyncio import sleep, Lock

from worker.models import Task, Queue, List


logger = logging.getLogger(__name__)


class Worker:
    """Worker for synchronous adding number num to list after waiting timeout seconds"""
    __worker_lock = Lock()

    @classmethod
    async def execute(cls, num: int, timeout: int):
        """Asynchronously create task and synchronously run worker"""
        task = Task(num=num, timeout=timeout)
        logger.info(f"Created task in worker {task=}")

        await Queue.append(task)
        logger.info(f"Added {task=} to the queue")

        async with cls.__worker_lock:
            logger.info(f"Waiting {timeout} seconds")
            await sleep(task.timeout)
            List.append(task.num)
            logger.info(f"{task=} completed")

            Queue.remove(task)
            logger.info(f"Removed {task=} from the queue")
