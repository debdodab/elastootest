import logging.config

from fastapi import FastAPI

from api.views import api_router

app = FastAPI()

logging.config.fileConfig("config/logging.conf", disable_existing_loggers=False)

app.include_router(api_router)
