import logging

from worker.models import Task, Queue, List


logger = logging.getLogger(__name__)


async def get_queue() -> list[Task]:
    return await Queue.get_all()


async def get_list() -> list[int]:
    return await List.get_all()
