import logging
from fastapi import APIRouter, BackgroundTasks

from api.services import get_queue, get_list
from worker.services import Worker

api_router = APIRouter()
logger = logging.getLogger(__name__)


@api_router.post("/task")
async def task_post(num: int, timeout: int, background_tasks: BackgroundTasks):
    background_tasks.add_task(Worker.execute, num, timeout)
    logger.info(f"Created worker with {num=} {timeout=}")
    return {"status": "ok"}


@api_router.get("/queue")
async def queue_get():
    return await get_queue()


@api_router.get("/list")
async def list_get():
    return await get_list()
